import React from "react";

type DisplayAreaProps = {
  display: string;
};
export function DisplayArea(props: DisplayAreaProps) {
  const { display } = props;
  return (
    <div className="flex w-auto h-16 border-b-2 border-black md:h-24">
      <span className="mt-auto mb-2 text-4xl ms-auto">{display}</span>
    </div>
  );
}

type CalcButtonProps = {
  children: string;
  onClick?: (n: string) => void;
};
export function CalcButton(props: CalcButtonProps) {
  const { children, onClick } = props;
  return (
    <button
      onClick={() => onClick?.(children)}
      className="w-16 h-16 md:w-24 md:h-24 hover:text-white hover:bg-gray-400"
    >
      <span className="text-2xl">{children}</span>
    </button>
  );
}

export function App() {
  const [formula, setFormula] = React.useState("");

  const onClickButton = (n: string) => {
    if (n === "C") {
      setFormula("");
    } else if (n === "=") {
      try {
        const ans = eval(formula);
        setFormula(ans);
      } catch (e) {}
    } else {
      setFormula(formula + n);
    }
  };

  return (
    <div className="flex flex-col min-h-screen">
      <div className="m-auto">
        <DisplayArea display={formula} />
        <div className="grid grid-cols-4 gap-1 m-1">
          <CalcButton onClick={onClickButton}>7</CalcButton>
          <CalcButton onClick={onClickButton}>8</CalcButton>
          <CalcButton onClick={onClickButton}>9</CalcButton>
          <CalcButton onClick={onClickButton}>/</CalcButton>

          <CalcButton onClick={onClickButton}>4</CalcButton>
          <CalcButton onClick={onClickButton}>5</CalcButton>
          <CalcButton onClick={onClickButton}>6</CalcButton>
          <CalcButton onClick={onClickButton}>*</CalcButton>

          <CalcButton onClick={onClickButton}>1</CalcButton>
          <CalcButton onClick={onClickButton}>2</CalcButton>
          <CalcButton onClick={onClickButton}>3</CalcButton>
          <CalcButton onClick={onClickButton}>-</CalcButton>

          <CalcButton onClick={onClickButton}>0</CalcButton>
          <CalcButton onClick={onClickButton}>C</CalcButton>
          <CalcButton onClick={onClickButton}>=</CalcButton>
          <CalcButton onClick={onClickButton}>+</CalcButton>
        </div>
      </div>
    </div>
  );
}
